<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/emballe_medias_spipmotion.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'em_spipmotion_description' => 'Complément vidéo pour "Emballe Medias" (utilise SPIPmotion)',
	'em_spipmotion_nom' => 'Emballe Medias - SPIPmotion',
	'em_spipmotion_slogan' => 'Complément vidéo pour "Emballe Medias" (utilise SPIPmotion)'
);
