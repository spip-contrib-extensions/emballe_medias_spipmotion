<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/em_spipmotion?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autre_version_format' => 'Este documento ha sido automáticamente codificado en @format@.',
	'autres_versions' => 'Otras versiones:',
	'autres_versions_formats' => 'Este documento se ha codificado en los formatos:',

	// I
	'info_encodage_pas_prevu' => 'Este documento no será codificado.',
	'info_previsu' => 'Previsualización',

	// L
	'lien_recharger_voir_player' => 'Recargar el lector',

	// M
	'message_document_attente_encodage' => 'Este documento está en la lista de espera de codificación.',
	'message_document_encours_encodage' => 'Este documento está en curso de codificación.',

	// T
	'title_infos_cacher' => 'Ocultar la información adicional',
	'title_infos_voir' => 'Ver la información adicional'
);
