<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/em_spipmotion?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autre_version_format' => 'This document has been automatically encoded in the @format@ format.',
	'autres_versions' => 'Other versions:',
	'autres_versions_formats' => 'This document was automatically encoded formats:',

	// I
	'info_encodage_pas_prevu' => 'This document won’t be encoded.',
	'info_previsu' => 'Preview',

	// L
	'lien_recharger_voir_player' => 'Reload the player',

	// M
	'message_document_attente_encodage' => 'This document is in the encoding queue.',
	'message_document_encours_encodage' => 'This document is in the encoding process.',

	// T
	'title_infos_cacher' => 'Hide additional information',
	'title_infos_voir' => 'Show additional information'
);
