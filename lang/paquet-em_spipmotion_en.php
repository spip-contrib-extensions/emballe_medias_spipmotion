<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-em_spipmotion?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'em_spipmotion_description' => 'Video add-on for "Wrap media" (use SPIPmotion)',
	'em_spipmotion_nom' => 'Wrap medias - SPIPmotion',
	'em_spipmotion_slogan' => 'Video add-on for "Wrap media" (use SPIPmotion)'
);
