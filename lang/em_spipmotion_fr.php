<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/emballe_medias_spipmotion.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'autre_version_format' => 'Ce document a été automatiquement encodé en @format@.',
	'autres_versions' => 'Autres versions :',
	'autres_versions_formats' => 'Ce document a été automatiquement encodé aux formats : ',

	// I
	'info_encodage_pas_prevu' => 'Ce document ne sera pas encodé.',
	'info_previsu' => 'Prévisualisation',

	// L
	'lien_recharger_voir_player' => 'Recharger le lecteur',

	// M
	'message_document_attente_encodage' => 'Ce document est dans la file d’attente d’encodage.',
	'message_document_encours_encodage' => 'Ce document est en cours d’encodage.',

	// T
	'title_infos_cacher' => 'Cacher les informations supplémentaires',
	'title_infos_voir' => 'Voir les informations supplémentaires'
);
