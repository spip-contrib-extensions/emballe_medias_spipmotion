<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-em_spipmotion?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'em_spipmotion_description' => 'Complemento vídeo para "Empaqueta Medias" (utiliza SPIPmotion)',
	'em_spipmotion_nom' => 'Empaqueta Medias - SPIPmotion',
	'em_spipmotion_slogan' => 'Complemento vídeo para "Empaqueta Medias" (utiliza SPIPmotion)'
);
